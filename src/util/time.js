export const timeAgo = time => {
	const timestmap = Date.parse(time),
		dis = (Date.now() - timestmap) / 1000
	if (dis < 60 * 60) {
		return ~~(dis / 60) + '分钟之前'
	}
	if (dis < 60 * 24 * 60) {
		return ~~(dis / 3600) + '小时之前'
	}
	if (dis > 60 * 24 * 60) {
		return ~~(dis / 24 / 60 / 60) + '天前'
	}
}
