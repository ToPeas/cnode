import { h, Component } from 'preact'
import style from './style.less'
import { Link } from 'preact-router'
import { timeAgo} from '../../util/time'

export default class List extends Component {

	// shouldComponentUpdate(){
	// 	console.log(this.base)
	// 		return true
	// }
	render({ list }, {}) {
	// console.log(this)
		return (
			<div class={style.list}>
				<section class="list">
					<Link href={`/user/${list.author.loginname}`}><img src={list.author.avatar_url} alt=""/></Link>
					<h4><Link href={`/topic/${list.id}`}>{list.title}</Link></h4>
					<div class="time">{timeAgo(list.last_reply_at)}</div>
				</section>

			</div>
		)
	}
}
