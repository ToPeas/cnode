import {h, Component} from 'preact'
import {Link} from 'preact-router/match'
import style from './style.less'

export default class Header extends Component {
		render() {
				return (
						<div class={style.nav}>
								<Link activeClassName="active" href="/">
										<span>全部</span>
								</Link>
								<Link activeClassName="active" href="/share">
										<span>分享</span>
								</Link>
								<Link activeClassName="active" href="/good">
										<span>精华</span>
								</Link>
								<Link activeClassName="active" href="/ask">
										<span>问答</span>
								</Link>
						</div>
				)
		}
}
