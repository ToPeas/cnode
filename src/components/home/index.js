import {h, Component} from 'preact'
import {Link} from 'preact-router'
import style from './style.less'
import List from '../lists'
import fetch from '../../api'

export default class Home extends Component {
		state = {
				lists: [],
				activeItem: 'all',
				page: 1
		}
		fetchData = ({
				tab,
				limit = 15,
				page = 1
		}) => {
				fetch({
						method: 'get',
						url: 'topics',
						params: {
								page,
								tab,
								limit
						}
				}).then(response => {
						this.setState({lists: response.data.data, page: page, activeItem: tab})
				})
		}

		componentDidMount() {
				const {
						page = 1,
						limit = 15,
						tab = 'all'
				} = this.props
				this.fetchData({tab, page, limit})
		}
		// componentWillMount(prev,next){ 	console.log(this.props); }

		componentWillUpdate(props, state) {
				const nextTab = props.tab
				const prevTab = this.props.tab
				if (nextTab !== prevTab) {
						this.fetchData({tab: nextTab})
				}
		}

		handleClick = val => {
				this.fetchData({tab: val})
				this.setState({activeItem: val})
		}
		next = () => {
				const {
						page = 1
				} = this.state
				this.fetchData({
						page: page + 1,
						tab: this.state.activeItem
				})
		}

		prev = () => {
				const {
						page
				} = this.state
				this.fetchData({
						page: page - 1,
						tab: this.state.activeItem
				})
		}

		render(props, state) {
				const {page} = this.state
				return (
						<div class={style.home}>
								<List lists={state.lists}/>
								<div className={style.pagination}>
										<button onClick={this.next}>下一页</button>
										<span>
												当前是第:{this.state.page}
												页</span>
										{page !== 1
												? <button onClick={this.prev}>
																上一页
														</button>
												: ''}
								</div>

						</div>
				)
		}
}