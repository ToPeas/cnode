import { h, Component } from 'preact'
import style from './style.less'
import List from '../list'

export default class Lists extends Component {
	render({ lists }, {}) {
		const listItems = lists.map ((item, index) =>
			<List key={index} list={item}/>
		)
		return (
			<div class={style.lists}>
				<ul>
					{listItems}
				</ul>
			</div>
		)
	}
}
