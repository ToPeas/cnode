import { h, Component } from 'preact'
import style from './style.less'
import fetch from '../../api'
import { Link } from 'preact-router'
// import { Provider, connect } from 'preact-redux';


export default class Topic extends Component {
	state = {
		author: {
			loginname: '',

		},
		replies: [],
		liked: false,
	}

	componentDidMount() {
		const { id } = this.props
		fetch ({
			method: 'get',
			url: `topic/${id}`
		}).then (response => {
			this.setState ({ ...response.data.data })
			return Promise.resolve ()
		}).then (() => {
			return fetch ({
				method: 'get',
				url: `topic_collect/${'ToPeas'}`,
			})
		}).then (res => {
			this.setState ({
				liked: res.data.data.some (item => {
					return item.id === this.props.id
				})
			})
		})
	}

	handleLikedClick = () => {
		afetch ({
			method: 'post',
			url: `/topic_collect/collect`,
			data: {
				topic_id: this.props.id
			}
		}).then ((res) => {
			this.setState ({ liked: true })
			return JSON.stringify (res)
		})
		// console.log (`%c ${res}`, 'font-size: 20px; color: #fff')
	}

	handleUnLikedClick = () => {
		fetch ({
			method: 'post',
			url: `/topic_collect/de_collect`,
			data: {
				topic_id: this.props.id
			}
		}).then (() => {
			this.setState ({ liked: false })
		})
	}

	render(props, state) {
		const { title, content, liked } = state
		const InnerHTMLHelper = ({ tagName, html }) =>
			h (tagName, { dangerouslySetInnerHTML: { __html: html } })
		return (
			<div class={style.topic}>
				<div>
					<header>
						<h3>{title}
							{!liked ? <button onClick={this.handleLikedClick} style="float:right">点击收藏</button> :
								<button onClick={this.handleUnLikedClick} style="float:right">取消收藏</button>}
						</h3>
						<h5><span>发布于{state.create_at}-</span>
							<span>作者:{state.author.loginname}-</span>
							<span>{state.visit_count}次浏览</span>
						</h5>
					</header>
					<InnerHTMLHelper tagName='article' html={content}/>
					<div>
						{
							state.replies.map ((item, index) => {
								return (
									<div class={style.reply}>
										<div class={style.avatar}>
											<Link href={`/user/${item.author.loginname}`}> <img src={item.author.avatar_url} alt=""/></Link>
										</div>
										<div>
											<h4>{item.author.loginname}:<a id={item.id}
																										 href={'#' + item.id}>{index + 1}楼
												: {item.create_at}</a></h4>
											<InnerHTMLHelper tagName='article' html={item.content}/>
										</div>
									</div>
								)
							})
						}
					</div>
				</div>
			</div>
		)
	}
}
