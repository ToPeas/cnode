import { h, Component } from 'preact'
import style from './style.less'
import fetch from '../../api'

export default class User extends Component {
	state = {}

	componentDidMount() {
		const { loginname } = this.props
		fetch ({
			method: 'get',
			url: `/user/${loginname}`
		}).then (response => {
			this.setState ({ ...response.data.data })
		}).catch (err => console.log (err))

	}

	render(props, state) {
		return (
			<div class={style.user}>
				{state.loginname ? (<div><img src={state.avatar_url} alt=""/>
					<h3>用户名：{props.loginname}</h3>
					<h3>github：{state.githubUsername}</h3>
					<h3>score：{state.score}</h3></div>) : ''
				}
			</div>

		)
	}
}
