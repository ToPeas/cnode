import {h, Component} from 'preact'
import {Router, Link} from 'preact-router'
import fetch from '../api'

import Header from './header'
import Home from './home'
import Profile from './profile'
import Topic from './topic'
import User from './user'

const Err = () => {
		return (
				<div>404</div>
		)
}

export default class App extends Component {
		/** Gets fired when the route changes.
	 *  @param {Object} event    "change" event from [preact-router](http://git.io/preact-router)
	 *  @param {string} event.url  The newly routed URL
	 */
		state = {
				showMenu: true
		}
		changeRoutes = (e) => {
				const url = e.url
				if (['/', '/share', '/good', 'ask'].includes(url)) {
						return this.setState({showMenu: true})
				}
				return this.setState({showMenu: false})

		}
		render() {
				return (
						<div id="app">
								<div class='header'>
										<header>
												<h1>
														<Link href="/">CNode</Link>
												</h1>
												<nav>
														<Link href="/">首页</Link>
												</nav>
										</header>
								</div>
								{this.state.showMenu
										? < Header />
										: null}
								<Router onChange={this.changeRoutes}>
										<Home path="/"></Home>
										<Home path="/good" tab="good"/>
										<Home path="/share" tab="share"/>
										<Home path="/ask" tab="ask"/>
										<Topic path="/topic/:id"/>
										<User path="/user/:loginname"/>
										<Err path="/404" default/>
								</Router>
						</div>
				)
		}
}