import axios from 'axios'

const instance = axios.create ({
	baseURL: ' https://cnodejs.org/api/v1',
	responseType: 'json',
	timeout: 10000,
})

instance.interceptors.request.use (config => {

	if (config.method === 'post' || config.method === 'put' || config.method === 'delete') {
		config.data = { ...config.data, accesstoken: 'efbfb8e5-1416-4290-b704-67852600a903' }
	}
	// if (localStorage.getItem ('token')) {
	// 	config.headers.Authorization = localStorage.getItem ('token')
	// }
	// config.headers.Authorization = ''

	return config

}, err => console.log (err))

instance.interceptors.response.use (response => {

	// if (!response.data || !response.data.success) {
	// 	// console.log(response)
	// 	// debugger
	// 	console.log ('接口返回有错误')
	// 	return Promise.reject (response.data, message)
	// }

	return response

}, err => {
	console.log ('返回值有错')
})

export default instance
